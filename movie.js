function createResultTable(data) {
  //Empty table
  let movieTable = document.getElementById('movieTable');
  if (movieTable != null) {
    movieTable.remove();
  }

  data.then((json) => {
    for (let i = 0; i < json.length; i++) {
      let currentData = json[i];
      let dataRow = document.createElement('tr');
      tableNode.appendChild(dataRow);
  
      let columnNode = null;
      columnNode = document.createElement('td');
      columnNode.innerText = currentData['name'];
      dataRow.appendChild(columnNode);

      columnNode = document.createElement('td');
      columnNode.innerText = currentData['synopsis'];
      dataRow.appendChild(columnNode);

      columnNode = document.createElement('td');
      let imageNode = document.createElement('img');
      imageNode.setAttribute('src', currentData['imageUrl']);
      imageNode.style.width = '200px';
      imageNode.style.height = '200px';
      dataRow.appendChild(imageNode);
    }
  })

  let resultElement = document.getElementById('resultTable');
  let tableNode = document.createElement('table');
  tableNode.setAttribute('id', 'movieTable');
  resultElement.appendChild(tableNode);
  tableNode.setAttribute('class', 'table');

  //Create the header
  let tableHeadNode = document.createElement('thead');
  tableNode.appendChild(tableHeadNode);
  let tableRowNode = document.createElement('tr');
  tableHeadNode.appendChild(tableRowNode);

  tableHeaderNode = document.createElement('th');
  tableHeaderNode.setAttribute('scope', 'col');
  tableHeaderNode.innerText = 'Name';
  tableHeadNode.appendChild(tableHeaderNode);

  tableHeaderNode = document.createElement('th');
  tableHeaderNode.setAttribute('scope', 'col');
  tableHeaderNode.innerText = 'Synopsis';
  tableHeadNode.appendChild(tableHeaderNode);

  tableHeaderNode = document.createElement('th');
  tableHeaderNode.setAttribute('scope', 'col');
  tableHeaderNode.innerText = 'image';
  tableHeadNode.appendChild(tableHeaderNode);
}

async function loadAllMoviesAsync() {
  let response = await fetch('https://dv-excercise-backend.appspot.com/movies');
  let data = await response.json();
  return data;
}

async function loadOneMovie() {
  let movieName = document.getElementById('queryId').value;
  if (movieName != '' && movieName != null) {
    let response = await fetch(`https://dv-excercise-backend.appspot.com/movies/${movieName}`);
    if (response.status == 200) {
      return await response.json();
    }
  }
}