"use strict";

function loadAllStudents() {
  var responseData = null;
  var data = fetch('https://dv-student-backend-2019.appspot.com/students')
      .then((response) => {
        console.log(response);
        return response.json();
      }).then((json) => {
        responseData = json;
        var resultElement = document.getElementById('result');
        resultElement.innerHTML = JSON.stringify(responseData, null, 2);
      });
}

async function loadAllStudentsAsync() {
  let response = await fetch('https://dv-student-backend-2019.appspot.com/students');
  let data = await response.json();
  // var resultElement = document.getElementById('result');
  // resultElement.innerHTML = JSON.stringify(data, null, 2);
  return data;
}

function createResultTable(data) {
  //Empty table
  let studentTable = document.getElementById('studentTable');
  if (studentTable != null) {
    studentTable.remove();
  }

  data.then((json) => {
    console.log(data);
    for (let i = 0; i < json.length; i++) {
      let currentData = json[i];
      let dataRow = document.createElement('tr');
      tableNode.appendChild(dataRow);
      let dataFirstColumnNode = document.createElement('th');
      dataFirstColumnNode.setAttribute('scope', 'row');
      dataFirstColumnNode.innerText = currentData['id'];
      dataRow.appendChild(dataFirstColumnNode);
  
      let columnNode = null;
      columnNode = document.createElement('td');
      columnNode.innerText = currentData['studentId'];
      dataRow.appendChild(columnNode);
  
      columnNode = document.createElement('td');
      columnNode.innerText = currentData['name'];
      dataRow.appendChild(columnNode);
  
      columnNode = document.createElement('td');
      columnNode.innerText = currentData['surname'];
      dataRow.appendChild(columnNode);
  
      columnNode = document.createElement('td');
      columnNode.innerText = currentData['gpa'];
      dataRow.appendChild(columnNode);
  
      columnNode = document.createElement('td');
      let imageNode = document.createElement('img');
      imageNode.setAttribute('src', currentData['image']);
      imageNode.style.width = '200px';
      imageNode.style.height = '200px';
      dataRow.appendChild(imageNode);
    }
  })

  let resultElement = document.getElementById('resultTable');
  let tableNode = document.createElement('table');
  tableNode.setAttribute('id', 'studentTable');
  resultElement.appendChild(tableNode);
  tableNode.setAttribute('class', 'table');

  //Create the header
  let tableHeadNode = document.createElement('thead');
  tableNode.appendChild(tableHeadNode);
  let tableRowNode = document.createElement('tr');
  tableHeadNode.appendChild(tableRowNode);
  let tableHeaderNode = document.createElement('th');
  tableHeaderNode.setAttribute('scope', 'col');
  tableHeaderNode.innerText = "#";
  tableHeadNode.appendChild(tableHeaderNode);

  tableHeaderNode = document.createElement('th');
  tableHeaderNode.setAttribute('scope', 'col');
  tableHeaderNode.innerText = 'studentId';
  tableHeadNode.appendChild(tableHeaderNode);

  tableHeaderNode = document.createElement('th');
  tableHeaderNode.setAttribute('scope', 'col');
  tableHeaderNode.innerText = 'name';
  tableHeadNode.appendChild(tableHeaderNode);

  tableHeaderNode = document.createElement('th');
  tableHeaderNode.setAttribute('scope', 'col');
  tableHeaderNode.innerText = 'surname';
  tableHeadNode.appendChild(tableHeaderNode);

  tableHeaderNode = document.createElement('th');
  tableHeaderNode.setAttribute('scope', 'col');
  tableHeaderNode.innerText = 'gpa';
  tableHeadNode.appendChild(tableHeaderNode);

  tableHeaderNode = document.createElement('th');
  tableHeaderNode.setAttribute('scope', 'col');
  tableHeaderNode.innerText = 'image';
  tableHeadNode.appendChild(tableHeaderNode);
}

async function loadOneStudent() {
  let studentId = document.getElementById('queryId').value;
  if (studentId != '' && studentId != null) {
    let response = await fetch(`https://dv-student-backend-2019.appspot.com/students/${studentId}`);
    if (response.status == 200) {
      return new Array(await response.json());
    }
  }
}